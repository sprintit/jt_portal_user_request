# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2019-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
	'name': 'Portal User Request',
	'summary': 'Approve/create portal user if is valid.',
	'author': 'Jupical Technologies Pvt. Ltd.',
	'version': '14.0.0.1',
	'summary': '''The application to manage signup request from website''',
	'category': 'Extra Tools',
	'website': 'http://www.jupical.com',
	'depends': ['website', 'helpdesk', 'auth_signup'],
	'data': [
		'views/helpdesk_ticket_view.xml',
		'views/template.xml'
	],
	'license': 'LGPL-3',
	'installable': True,
        'application': True,
        'images': ['static/description/poster_image.png'],
	'price': 20.00,
	'currency': 'USD',
}
