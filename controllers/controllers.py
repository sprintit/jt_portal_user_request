# -*- coding: utf-8 -*-
##############################################################################
#
#    Jupical Technologies Pvt. Ltd.
#    Copyright (C) 2019-TODAY Jupical Technologies(<http://www.jupical.com>).
#    Author: Jupical Technologies Pvt. Ltd.(<http://www.jupical.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import http
from odoo.http import request
from odoo.addons.auth_signup.controllers.main import AuthSignupHome


class WebsitePortalRequest(AuthSignupHome):

    @http.route('/web/signup', type='http', auth='public', website=True, sitemap=False)
    def web_auth_signup(self, *args, **kw):
        # super(WebsitePortalRequest, self).web_auth_signup(*args, **kw)
        return request.render('jt_portal_user_request.auth_signup_form')

    @http.route('/custom_signup', type='http', website=True, auth='public', csrf=False)
    def web_auth_signup_custom(self, **kw):

        if request.httprequest.method == 'POST' and kw.get('name') and kw.get('email') and kw.get('phone') and kw.get('company'):
            signup_dict = {}

            # Get Partner_user
            user = request.env['res.partner'].sudo().search([('name', '=', kw['name']), ('email', '=', kw['email'])], limit=1)
            if user:
                res_user = request.env['res.users'].sudo().search([('partner_id', '=', user.id)], limit=1)
                if res_user:
                    signup_dict.update({'signup_fail': "You are already member of our portal, please login to continue.."}) 
                else:
                    signup_dict.update({'signup_fail': "You have already submitted a signup request, We will contact you shortly.."})   
                return request.render('jt_portal_user_request.auth_signup_form', signup_dict)
            else:
                # Get/Create Company
                company = request.env['res.partner'].sudo().search([('name', '=', kw['company'])], limit=1)
                if not company:
                    company = request.env['res.partner'].sudo().create({
                        'name': kw['company'],
                        'company_type': 'company'
                    })
                # Create User
                user = request.env['res.users'].sudo().create({
                    'name': kw['name'],
                    'login': kw['email'],
                })
                if user:
                    user.write({'active': False})
                    user.partner_id.write({
                        'phone': kw['phone'],
                        'email': kw['email'],
                        'company_type': 'person',
                        'parent_id': company.id
                    })
           
                # Create Helpdesk Ticket
                ticket = request.env['helpdesk.ticket'].sudo().create({
                    'name': 'New portal user request',
                    'partner_id': user.partner_id.id,
                    'partner_email': user.partner_id.email,
                    'is_portal_request': True,
                })
                signup_dict.update({'signup_success': "Thank you for this request, We will contact you shortly.."})
                return request.render('jt_portal_user_request.auth_signup_form', signup_dict)
        # Render template without signup
        return request.render('jt_portal_user_request.auth_signup_form')
    